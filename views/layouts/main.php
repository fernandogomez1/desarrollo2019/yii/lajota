<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => [
            ['label' => '<span class="glyphicon glyphicon-home"></span> Inicio',
             'url' => ['/site/index'],
                ],
            
            ['label' => '<span class="glyphicon glyphicon-headphones"></span> Ofertas', 'url' => ['/site/ofertas']],
            ['label' => '<span class="glyphicon glyphicon-book"></span> Productos', 'url' => ['/site/productos']],
            ['label' => '<span class="glyphicon glyphicon-step-backward"></span> Categorias', 'url' => ['/site/categorias']],
            ['label' => '<span class="glyphicon glyphicon-screenshot"></span> Nosotros', 'url' => ['/site/nosotros'],'items' => [
                ['label' => 'Donde estamos', 'url' => ['site/nosotros/donde', 'tag' => 'new']],
                ['label' => 'Quienes somos', 'url' => ['site/nosotros/quienes', 'tag' => 'popular']],
                ['label' => 'Nuestros productos', 'url' => ['site/nosotros/productos', 'tag' => 'popular']],
                ['label' => 'Informacion', 'url' => ['site/nosotros/Informacion', 'tag' => 'popular']],
                
                ]],
            ['label' => '<span class=" 	glyphicon glyphicon-plane"></span> Contacto', 'url' => ['/site/contacto']],
        
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
