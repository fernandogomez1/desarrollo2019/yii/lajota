<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="tarea">
<h2>
<?= 
    //Visualizar ListView
    Html::encode($model->nombre) ?></h2>
<?= HtmlPurifier::process($model->descripcion) ?>
</div>