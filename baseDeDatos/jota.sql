-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-02-2020 a las 19:59:04
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `jota`
--
CREATE DATABASE IF NOT EXISTS `jota` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `jota`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `oferta` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `foto`, `descripcion`, `oferta`) VALUES
(1, 'producto1', 'foto1.jpg', 'Descripcion del producto blablablablalb jhklsdfjhksdfjhksdfjhksdfjh \r\nsd\r\nf\r\nasd\r\nfasdfjasdhfjaskdhfjaskdfhsdk', 1),
(2, 'producto2', 'foto2.jpg', 'fasfds  fasd fasd fasf hgsdf ghter dfs ghsdf gdfs ', 0),
(3, 'producto3', 'foto3.jpg', 'sdfg t ñlikuj ugk jkhf jgh ghfd gf s', 1),
(4, 'producto4', 'foto4.jpg', 'hsdfjghkdfjghkdfjghkdfjghkdfjahkjhkdf dfjhsjhkdf jhkasdfjhkldfjhkd hkljdf akls fja sjklf hañkjfhawiu ', 1),
(5, 'producto5', 'foto5.jpg', 'asdfghklerjklerjkl dajahkdjahskdf d jhkdjhkdjhjahkf', 0),
(6, 'producto6', 'foto6.jpg', 'fafasdhsghf ghsfasdjklfahsdyiuofejcwmfgsdafkgfdskjafgha', 1),
(7, 'producto7', 'foto7.jpg', 'ghfghs gsf hgfs hfj uytiuyir7u745 fds hsf h', 1),
(8, 'producto8', 'foto8.jpg', 'h twr hrh wrh65eu76ikuyfghydjtyejhgdf h sfhsh gfh try 6ry', 0),
(9, 'producto9', 'foto9.jpg', 'dfg sdf gtr ghsdfh trys ytru645 y6tresy htserh tsrh t', 0),
(10, 'producto10', 'foto10.jpg', 'sdfgh tsr hsgtdfh trs htry tdshgfsdhgsf hgfd hgs h', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
